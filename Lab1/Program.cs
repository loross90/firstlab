﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Program
    {
        public static List<Note> book = new List<Note>();
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Я записаня книжка.\nЧтобы завершить работу введите 0 и нажмите Enter\nЧтобы создать запись введите 1 и нажмите Enter\nЧтобы редактировать запись по ID введите 2 и нажмите Enter" +
                    "\nЧтобы показать все записи введите 3 и нажмите Enter\nЧтобы просмотреть конкретную запись по ID введите 4 и нажмите Enter\nЧтобы удалить конкретную запись по ID введите 5 и нажмите Enter");
                bool isNum = Int32.TryParse(Console.ReadLine(),out int a);
                if (isNum == false)
                {
                    Console.Clear();
                    Console.WriteLine("Введите цифру от 0 до 5 и нажмите Enter:");
                    continue;
                }
                if (a == 0)
                {
                    break;
                }
                Console.Clear();
                switch (a)
                {
                    case 1:
                        Console.WriteLine("Создание");
                        Methods.CreateNote();
                        break;
                    case 2:
                        Console.WriteLine("Редактирование");
                        Methods.ChangeNote();
                        break;
                    case 3:
                        Console.WriteLine("Показ всех записей");
                        Methods.ShowAll();
                        break;
                    case 4:
                        Console.WriteLine("Просмотр записи по ID");
                        Methods.FindNote();
                        break;
                    case 5:
                        Console.WriteLine("Удаление записи по ID");
                        Methods.DeleteNote();
                        break;
                    default:
                        Console.WriteLine("Введите нормальную команду");
                        break;
                }
            }
        }
    }
}
