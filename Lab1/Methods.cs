﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Methods
    {
        public static void CreateNote()
        {
            Note a = new Note();
            
            Console.WriteLine("Введите имя (обязательно)");
            a.FirstName = CheckEmpty(Console.ReadLine(),"имя:"); Console.Clear();
            Console.WriteLine("Введите фамилию (обязательно)");
            a.SecondName = CheckEmpty(Console.ReadLine(), "фамилию:"); Console.Clear();
            Console.WriteLine("Если хотите, введите отчество (не обязательно). Если не хотите вводить, оставьте поле пустым и нажмите Enter.");
            a.MiddleName = CaseEmpty(); Console.Clear();
            Console.WriteLine("Введите телефон (обязательно). Только цифры");
            while (true)
            {
                if (!Int32.TryParse(Console.ReadLine(), out int tel))
                {
                    Console.Clear();
                    Console.WriteLine("Введите телефон (обязательно). Телефон должен содержать только цифры.");
                }
                else
                {
                    Console.Clear();
                    a.PhoneNumber = tel;
                    break;
                }
            }
            Console.Clear();
            Console.WriteLine("Введите страну, в которой живет этот человек (обязательно)");
            a.Country = CheckEmpty(Console.ReadLine(), "страну:"); Console.Clear();
            Console.WriteLine("Будете ли вы вводить дату рождения человека? Если не будете, оставьте поле пустым и нажмите Enter. Если да, напишите что нибудь и нажмите Enter.");
            string bd = Console.ReadLine();
            if (bd == "" || bd == null)
            {
                a.BirhtDate = "Не указано";
            }
            else
            {
                a.BirhtDate = SetBirthDate().ToLongDateString().ToString();
            }
            Console.Clear();
            Console.WriteLine("Введите имя организации, в которой работает этот человек (не обязательно). Если не хотите вводить, оставьте поле пустым и нажмите Enter.");
            a.Organization = CaseEmpty(); Console.Clear();
            Console.WriteLine("Введите название должности, которую занимает этот человек (не обязательно). Если не хотите вводить, оставьте поле пустым и нажмите Enter.");
            a.Position = CaseEmpty(); Console.Clear();
            Console.WriteLine("Введите любые заметки об этом человеке (не обязательно). Если не хотите вводить, оставьте поле пустым и нажмите Enter.");
            a.Notes = CaseEmpty(); Console.Clear();
            Program.book.Add(a);
        }

        public static string CaseEmpty()
        {
            string a = Console.ReadLine();
            if (a == "" || a == null)
            {
                return "Не указано";
            }
            return a;
        }

        public static string CheckEmpty(string s, string param)
        {
            while (true)
            {
                if (s == "")
                {
                    Console.Clear();
                    Console.WriteLine($"Введите {param}");
                    s = Console.ReadLine();
                }
                else if (s.IndexOf(' ') > -1)
                {
                    Console.Clear();
                    Console.WriteLine($"Здесь не должно быть пробелов. Введите {param}\b заново.");
                    s = Console.ReadLine();
                }
                else
                {
                    break;
                }
            }
            Console.Clear();
            return s;
        }

        public static DateTime SetBirthDate()
        {
            int month, day, year;
            DateTime birthdate = new DateTime();

            while (true)
            {
                bool flag = true;
                while (true)
                {
                    Console.WriteLine("Введите номер месяца рождения человека (от 1 до 12)");
                    if (!Int32.TryParse(Console.ReadLine(), out month))
                    {
                        Console.Clear();
                    }
                    else if (month < 1 || month > 12)
                    {
                        Console.Clear();
                    }
                    else
                    {
                        Console.Clear(); break;
                    }
                }
                while (true)
                {
                    Console.WriteLine("Введите год рождения человека целым числом");
                    if (!Int32.TryParse(Console.ReadLine(), out year))
                    {
                        Console.Clear();
                    }
                    else if (year < 1900 || year > 2020)
                    {
                        Console.Clear();
                    }
                    else
                    {
                        Console.Clear(); break;
                    }
                }
                while (true)
                {
                    Console.WriteLine("Введите дату рождения человека целым числом");
                    if (!Int32.TryParse(Console.ReadLine(), out day))
                    {
                        Console.Clear();
                    }
                    else
                    {
                        break;
                    }
                }
                try
                {
                    birthdate = new DateTime(year, month, day);
                }
                catch (Exception)
                {
                    Console.WriteLine("Дата не существет, введите существующую дату.");
                    flag = false;
                }
                if (flag)
                {
                    Console.Clear();
                    break;
                }
            }
            return new DateTime(year,month,day);
        }

        public static void ShowAll()
        {
            Console.Clear();
            if (Program.book.Count == 0)
            {
                Console.WriteLine($"Записей нет, просмотр невозможен. Нажмите Enter для продолжения.");
                Console.ReadKey();
                Console.Clear();
                return;
            }
            Console.WriteLine("Все доступные записи:");
            foreach (var item in Program.book)
            {
                Console.WriteLine($"ID: {item.Id}\nИмя: {item.FirstName}\nФамилия: {item.SecondName}\nНомер телефона: {item.PhoneNumber}");
            }
            Console.ReadKey();
            Console.Clear();
        }

        public static void DeleteNote()
        {
            int Id = CheckExist("удаление невозможно", "удалить");
            if (Id == -1)
            {
                return;
            }
            Console.Clear();
            Program.book.Remove(Program.book.Find(x => x.Id == Id));
            Console.WriteLine("Удаление прошло успешно.");
            Console.ReadKey();
            Console.Clear();
        }

        public static void ChangeNote()
        {
            int Id = CheckExist("изменение записи невозможно", "изменить");
            if (Id == -1)
            {
                return;
            }
            var currentNote = Program.book.Find(x => x.Id == Id);
            Console.Clear();
            ShowNote(currentNote);
            Console.WriteLine("Хотите изменить имя? Нажмите Y если да и N если нет.");
            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                Console.Clear(); Console.WriteLine("\nВведите имя (обязательно)");
                currentNote.FirstName = CheckEmpty(Console.ReadLine(), "имя:");
            }
            Console.Clear(); ShowNote(currentNote); Console.WriteLine("Хотите изменить фамилию? Нажмите Y если да и N если нет.");
            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                Console.WriteLine("\nВведите фамилию (обязательно)");
                currentNote.SecondName = CheckEmpty(Console.ReadLine(), "фамилию:");
            }
            Console.Clear(); ShowNote(currentNote); Console.WriteLine("Хотите изменить отчество? Нажмите Y если да и N если нет.");
            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                Console.WriteLine("\nЕсли хотите, введите отчество (не обязательно). Если не хотите вводить, оставьте поле пустым и нажмите Enter.");
                currentNote.MiddleName = CaseEmpty();
            }
            Console.Clear(); ShowNote(currentNote); Console.WriteLine("Хотите изменить телефон? Нажмите Y если да и N если нет.");
            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                Console.WriteLine("\nВведите телефон (обязательно). Телефон должен содержать только цифры.");
                while (true)
                {
                    if (!Int32.TryParse(Console.ReadLine(), out int tel))
                    {
                        Console.Clear();
                        Console.WriteLine("Введите телефон (обязательно) Только цифры");
                    }
                    else
                    {
                        Console.Clear();
                        currentNote.PhoneNumber = tel;
                        break;
                    }
                }
            }
            Console.Clear(); ShowNote(currentNote); Console.WriteLine("Хотите изменить страну проживания человека? Нажмите Y если да и N если нет.");
            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                Console.WriteLine("\nВведите страну, в которой живет этот человек (обязательно).");
                currentNote.Country = CheckEmpty(Console.ReadLine(), "страну:");
            }
            Console.Clear(); ShowNote(currentNote); Console.WriteLine("Хотите изменить дату рождения человека? Нажмите Y если да и N если нет.");
            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                Console.WriteLine("\nВведите дату рождения человека. Если не хотите вводить, оставьте поле пустым и нажмите Enter. Если собираетесь вводить, напишите что нибудь и нажмите Enter.");
                string bd = Console.ReadLine();
                if (bd == "" || bd == null)
                {
                    currentNote.BirhtDate = "Не указано";
                }
                else
                {
                    currentNote.BirhtDate = SetBirthDate().ToLongDateString().ToString();
                }
            }
            Console.Clear(); ShowNote(currentNote); Console.WriteLine("Хотите изменить имя организации, в которой работает этот человек? Нажмите Y если да и N если нет.");
            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                Console.WriteLine("\nВведите имя организации, в которой работает этот человек (не обязательно)");
                currentNote.Organization = CaseEmpty();
            }
            Console.Clear(); ShowNote(currentNote); Console.WriteLine("Хотите изменить название должности, которую занимает этот человек? Нажмите Y если да и N если нет.");
            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                Console.WriteLine("\nВведите название должности, которую занимает этот человек (не обязательно)");
                currentNote.Position = CaseEmpty();
            }
            Console.Clear(); ShowNote(currentNote); Console.WriteLine("Хотите изменить любые заметки об этом человеке? Нажмите Y если да и N если нет.");
            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                Console.WriteLine("\nВведите любые заметки об этом человеке (не обязательно)");
                currentNote.Notes = CaseEmpty();
            }
            Console.Clear(); ShowNote(Program.book.Find(x => x.Id == Id));
            Console.WriteLine("Резактирование завершено. Нажмите любую клавишу.");
            Console.ReadKey();
            Console.Clear();
        }

        public static int CheckExist(string existance, string action)
        {
            while (true)
            {
                if (Program.book.Count == 0)
                {
                    Console.WriteLine($"Записей нет, {existance}. Нажмите Enter для продолжения.");
                    Console.ReadKey();
                    Console.Clear();
                    return -1;
                }
                Console.WriteLine($"Введите Id записи в диапазоне от 1 до {Program.book.Count}, чтобы её {action}.");
                if (!Int32.TryParse(Console.ReadLine(), out int Id))
                {
                    Console.WriteLine("Введено что то кроме цифр.");
                }
                else if (!Program.book.Exists(x => x.Id == Id))
                {
                    Console.WriteLine($"Такого ID нет. Невозможно {action} несуществующее. Нажмите Enter для продолжения.");
                    Console.ReadKey();
                    Console.Clear();
                }
                else
                {
                    return Id;
                }
            }
        }

        public static void FindNote()
        {
            int Id = CheckExist("просмотр невозможен", "просмотреть");
            if (Id == -1)
            {
                return;
            }
            Console.WriteLine("Информация по записи:");
            var a = Program.book.Find(x => x.Id == Id);
            ShowNote(a);
            Console.ReadKey();
            Console.Clear();
        }
        public static void ShowNote(Note currentNote)
        {
            Console.WriteLine($"ID записи: {currentNote.Id}\nИмя: {currentNote.FirstName}\nФамилия: {currentNote.SecondName}\nОтчество: {currentNote.MiddleName}\nТелефон: {currentNote.PhoneNumber}\n" +
                $"Страна: {currentNote.Country}\nДата рождения: {currentNote.BirhtDate}\nОрганизация: {currentNote.Organization}\nДолжность: {currentNote.Position}\nЗаметки: {currentNote.Notes}");
        }
    }
}
