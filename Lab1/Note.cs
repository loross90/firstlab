﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Note
    {
        internal static int quantity = 0;
        public string FirstName { set; get; }    //Имя
        public string SecondName { set; get; }   //Фамилия
        public string MiddleName { set; get; }   //Отчество-
        public int PhoneNumber { set; get; }     //Телефон (цифры)
        public string Country { set; get; }      //Страна
        public string BirhtDate { set; get; }    //Дата рождения-
        public string Organization { set; get; } //Организация-
        public string Position { set; get; }     //Должность-
        public string Notes { set; get; }        //Заметки-
        public int Id { set; get; }              //Айдишник

        public Note()
        {
            quantity++;
            Id = quantity;
        }
    }
}
